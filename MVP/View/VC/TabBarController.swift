//
//  TabBarController.swift
//  MVP
//
//  Created by diwka on 25/6/22.
//

import Foundation
import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBarItems()
    }
    
    private func configureTabBarItems() {
        let listVC = ViewController()
        let listPresenter = BeerListPresenter(view: listVC)
        listVC.presenter = listPresenter
        listVC.tabBarItem = UITabBarItem(title: "Beer List", image: UIImage(systemName: "1.circle"), tag: 0)

        let searchVC = SearchVC()
        let searchPresenter = SearchBeerPresenter(view: searchVC)
        searchVC.presenter = searchPresenter
        searchVC.tabBarItem = UITabBarItem(title: "Search Beer", image: UIImage(systemName: "2.circle"), tag: 1)
        
        let randomVc = RandomVC()
        let randomPresenter = RandomPresenter(view: randomVc)
        randomVc.presenter = randomPresenter
        randomVc.tabBarItem = UITabBarItem(title: "Random Beer", image: UIImage(systemName: "3.circle"), tag: 2)
        
        let listNavVC = UINavigationController(rootViewController: listVC)
        let searchNavVC = UINavigationController(rootViewController: searchVC)
        let randomvc = UINavigationController(rootViewController: randomVc)
        setViewControllers([listNavVC, searchNavVC, randomVc], animated: true)
        
    }
    
}
