//
//  NetworkApi.swift
//  mvpApp
//
//  Created by jojo on 22/6/22.
//

import Foundation

protocol NetworkService {
    func getBeerList(completion: @escaping ([Beer]) -> ())
    func getOneBeer(id: String, completion: @escaping ([Beer]) -> ())
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> ())
    func randomBeer(completion: @escaping ([Beer]) -> ())
}

class NetworkApi: NetworkService{
    
    let session = URLSession.shared
    let baseURL = "https://api.punkapi.com/v2/beers"
    
    func getBeerList(completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)?page=2&per_page=30"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func getOneBeer(id: String,completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)/\(id)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)?ids=\(id)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func randomBeer(completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)/random"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { data, _, _ in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
}
