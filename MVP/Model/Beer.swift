//
//  Beer.swift
//  MVP
//
//  Created by diwka on 22/6/22.
//

import Foundation

struct Beer: Codable {
    
    var id: Int?
    var name: String?
    var description: String?
    var imageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageURL = "image_url"
    }
    
}
