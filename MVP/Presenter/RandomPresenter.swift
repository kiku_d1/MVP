//
//  RandomPresenter.swift
//  MVP
//
//  Created by diwka on 29/6/22.
//

import Foundation

protocol RandomDelegate: AnyObject{
    init(view: RandomVC)
    func random()
}
class RandomPresenter: RandomDelegate {
    
    private weak var view: RandomVC?
    
    let networkApi: NetworkService!
    
    required init(view: RandomVC) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func random() {
        networkApi.randomBeer { beers in
            self.view?.randomBeer(beers: beers)
        }
    }
}

