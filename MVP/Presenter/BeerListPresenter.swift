//
//  BeerListPresenter.swift
//  MVP
//
//  Created by diwka on 22/6/22.
//

import Foundation

protocol BeerListViewPresenter: AnyObject {
    init(view: BeerListView)
    func viewDidLoad()
    func presentBeers()
}

class BeerListPresenter: BeerListViewPresenter  {
    
    private weak var view: BeerListView?
    private let networkApi: NetworkService!
    var beersList: [Beer] = []
    
    required init(view: BeerListView) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func viewDidLoad() {
        networkApi.getBeerList { [weak self] newBeers in
            self?.beersList = newBeers
            self?.view?.presentBeerList()
        }
    }
    
    func presentBeers() {
        view?.presentBeerList()
    }
}



