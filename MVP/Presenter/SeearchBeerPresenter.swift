//
//  SeearchBeerOresenter.swift
//  MVP
//
//  Created by diwka on 29/6/22.
//

import Foundation

protocol SerchBeerViewDelegate: AnyObject {
    init(view: SearchView)
    func search(id: Int)
}

class SearchBeerPresenter: SerchBeerViewDelegate {
    
    private weak var view: SearchView?
    
    let networkingApi: NetworkService!
    
    required init(view: SearchView) {
        self.view = view
        self.networkingApi = NetworkApi()
    }
    
    func search(id: Int) {
        networkingApi.searchBeer(id: id, completion: {[weak self] beers in
            self?.view?.onItemsRetrieval(beers: beers)
        })
    }
    
    
}
